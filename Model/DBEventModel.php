<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8';
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            try {
                 $this->db = new PDO($dsn, DB_USER, DB_PWD, $options);
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
            }
          }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Handle the Exception in a nicer fashion
      * @throws PDOException
      */
    public function getEventArchive()
    {
      $eventList = array();
      try {
        $stmt = $this->db->query('SELECT * FROM event ORDER BY id');
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          $eventList[] = new Event($row['title'], $row['date'], $row['description'], $row['id']);
        }
      } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage());
      }
      return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo   Handle the Exception in a nicer fashion.
      * @throws PDOException
      */
    public function getEventById($id)
    {
       $this->verifyId($id);
        $event = null;
          $stmt = $this->db->prepare('SELECT * FROM event WHERE id = :id');
          $stmt->bindValue(":id", $id);
          $stmt->execute();
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          $event = new Event($row['title'], $row['date'], $row['description'], $row['id']);

        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        $event->verify();

        $stmt = $this->db->prepare('INSERT INTO event (title, date, description) '. 'VALUES (:title, :date, :description)');
        $stmt->bindValue('title', $event->title);
        $stmt->bindValue('date', $event->date);
        $stmt->bindValue('description', $event->description);
        $stmt->execute();
        $event->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */

    public function modifyEvent($event)
    {
      $event->verify();
      $stmt = $this->db->prepare("UPDATE event SET title=:title, date=:date, description=:description WHERE id=:id");
      $stmt->bindValue('id', $event->id);
      $stmt->bindValue('title', $event->title);
      $stmt->bindValue('date', $event->date);
      $stmt->bindValue('description', $event->description);
      $stmt->execute();
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Handle exception nicer
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
      //sjekk gyldig i
        $this->verifyId($id);
        $stmt=$this->db->prepare("DELETE FROM event WHERE id= :id");
        $stmt->bindValue("id", $id);
        $stmt -> execute();


      }

      /** Helper function verifying that ids are numbers.
       * @param integer $id The event id to check.
       * @throws InvalidArgumentException if $id is not a number.
       * @static
       */
      public static function verifyId($id)
      {
          if (!is_numeric($id) || $id < 1) {
              throw new InvalidArgumentException
                        (self::MSG_TABLE[0]);
          }
      }

      public const MSG_TABLE = ['Event id expected to be a valid number. ',
                              'Event title is mandatory. ',
                              'Event date is mandatory. ',
                              'Date must be of format YYYY-MM-DD. ',
                              'Date must be an existing one. '
                          ];

    }
